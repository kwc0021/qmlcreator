QML Creator - advanced IDE for QML & JavaScript programming
=========

![QML](http://i.imgur.com/olMru1v.png)

QML Creator makes developing QML projects on mobile devices convenient.

It includes all the available Qt Quick modules and many original demo projects.

## Download

* [Android](https://play.google.com/store/apps/details?id=com.wearyinside.qmlcreator)
* [iOS](https://itunes.apple.com/us/app/qml-creator/id944301984)

## LICENSE

Copyright (C) 2013-2015 [Oleg Yadrov](https://linkedin.com/in/olegyadrov)

Distributed under the Apache Software License, Version 2.

---

## Note

Ported 2021 to Qt 5.13, currently tested on android only.

**N.B:** Release builds didn't work for me when using Qt Creator, so I needed
to build them on the command line (like with the android examples). The problem
with release builds in Qt Creator is that the example files don't get copied
(files size 0). This is probably a caching issue between debug and release
builds (just a poor guess).

## Storage

All examples and your own projects are stored in the globally accessible
directory `Documents/QML Projects/`.

If you want to use your own code, just push a directory containing your
code and a `main.qml` file from your desktop computer to the `Projects`
directory like so:
```
adb push MyProject /sdcard/Documents/QML\ Projects/Projects/
```

## Combine with CL REPL

You may use the following inside your QML:
```
import EQL5 1.0

   onClicked: Lisp.call("my:fun", arg)
```
Instead of calling into (the not available) Lisp, a message box will be shown
with the function name and the passed arguments. This is convenient if you want
to develop an UI to use from inside the CL REPL app (if you open a QML file in
the CL REPL, it will just display it, instead of opening it for editing).

Remember the following:

* always add (at least) a `Close` button as a child of the root item, to be
able to return to the CL REPL app:
```
  Button {
    anchors: horizontalCenter: parent.horizontalCenter
    text: "Close"
    onClicked: parent.visible = false
  }
```
If you want it fancier, you may add 3 buttons like this (should be self
explaining):
```
  Row {
    anchors.horizontalCenter: parent.horizontalCenter
    spacing: 5

    Button { // reload
      width: height
      text: "\uf021"
      font.family: fontAwesome.name
      font.pixelSize: 20
      onClicked: Lisp.call("eql:reload-qml-file")
    }
    Button { // minimize/maximize
      width: height
      text: canvas.visible ? "\uf2d1" : "\uf2d0"
      font.family: fontAwesome.name
      font.pixelSize: 20
      onClicked: canvas.visible = !canvas.visible
    }
    Button { // close
      width: height
      text: "\uf00d"
      font.family: fontAwesome.name
      font.pixelSize: 20
      onClicked: main.visible = false
    }
  }
```

## APK

A pre-built APK (kept up-to-date) of this fork of QML Creator is available on
[cl-repl.org](http://cl-repl.org)

