import QtQuick 2.10
import QtQuick.Controls 2.10
import QtQuick.Layouts 1.10

Item {
  anchors.fill: parent

  TabBar {
    id: bar
    width: parent.width

    TabButton { text: "Brightness contrast" }
    TabButton { text: "Desaturate" }
    TabButton { text: "Directional Blur" }
  }

  StackLayout {
    y: bar.height
    width: parent.width
    height: parent.height - y
    currentIndex: bar.currentIndex

    BrightnessContrastTab {}
    DesaturateTab {}
    DirectionalBlurTab {}
  }
}
